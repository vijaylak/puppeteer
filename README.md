# Chrome Dev Tools Testing using Puppeteer #

This project is designed to capture google ad network call invocation using Puppeteer and Jest 

### Set Up ###
Below are the steps to set up the project in a local machine:
* Clone the repository using the below command:
```bash
git clone https://vijaylak@bitbucket.org/vijaylak/puppeteer.git
```

* Download the node modules using:
```bash
npm install
```
* Run the tests using:
```bash
npm run test
```

### Tests ###
There are 2 primary tests to solve the question given. Below are the explanations:

* adinvoke.test.js : This test navigates to the page and then waits for the required request to be fired. Once the response of the request returns a 200, the status code is asserted and the next button is clicked. The page again waits for the request of the corresponding pattern and asserts the 200 status code.

* adinvokeinterceptor.test.js : This test makes use of the interception feature in the chrome dev tools protocol. The listener listens to all the requests fired from the page and resolves the promise when the corresponding request pattern is met. It returns a boolean to denote the interception and promise resolution. The boolean value is then asserted and the next button is clicked. The listener is again triggered to listen to the same pattern in the next page.

* mobile.test.js : This is the emulation of the above test in a mobile device.

### Reports ###
The test generates an allure report after the test is run and serves it in the browser. Remove the posttest script to avoid report serving.

### other Js files ###
* basepage.js : Contains the commonly used actions in the page that was tested. This can be scaled further to support new functionalities based on user need.

* selector.js : Contains all the selectors

* util.js: Contains Utility reusable functions

* jest.config.js : configuration for jest. The tests that need to be run are placed here.

* jest-puppeteer.config.js : contains all the configuration that enable the global variables (browser, page,etc) of puppeteer. All launch configurations can be adjusted here. 