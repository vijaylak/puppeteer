//Create a getURL to run as per the configuration

module.exports = {
  preset: "jest-puppeteer",
    globals: {
      URL: ""
    },
    testMatch: [
      "**/test/adinvoke2*.test.js"
    ],
    verbose: true,
    reporters: ["default", "jest-allure"],
    setupFilesAfterEnv : ['jest-allure/dist/setup'],
};