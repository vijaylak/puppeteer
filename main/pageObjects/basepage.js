
let getPageUrl = async (selector) => {
    
    await page.waitForSelector(selector);
    let pageUrl = await page.url();
    return pageUrl;
};

let elementClick = async (selector) => {
    await page.waitForSelector(selector);
    await page.focus(selector);
    await page.click(selector);
};

module.exports = {
    getPageUrl,elementClick
}