const puppeteer = require('puppeteer');
const assert = require('chai').assert;
const expect = require('chai').expect;
const {getPageUrl,elementClick} = require('../main/pageObjects/basepage');
const {sercall,resolveCall} = require('../main/util');
const selector = require ('../main/pageObjects/selector');
const timeout = 60000;

const devices = require('puppeteer/DeviceDescriptors');
const iPad = devices['iPad'];

beforeAll(async () => {
    await page.goto(URL,{waitUntil: 'domcontentloaded',timeout: 0});
},timeout);

describe('Testing in mobile',() =>{
    test('Mobile Test using emulator',async () =>{
                await page.emulate(iPad);
                //Check the call by waiting for the request
                let call1 = await sercall();
                //assert if the call returns a 200
                assert.equal(call1,200);
                //obtain the page url
                const url1 = await getPageUrl(selector.selector.next);
                console.log('URL 1: ',url1);
                //click the next page
                await elementClick(selector.selector.next);
                await page.waitFor(2000);
                //check if page has navigated
                const url2 = await getPageUrl(selector.selector.next);
                assert.notEqual(url1,url2);
                //Check google ads invocation
                let call2 = await sercall();
                assert.equal(call2,200);
    },timeout);
});
