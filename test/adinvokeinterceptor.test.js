const puppeteer = require('puppeteer');
const assert = require('chai').assert;
const expect = require('chai').expect;
const {getPageUrl,elementClick} = require('../main/pageObjects/basepage');
const {sercall,resolveCall} = require('../main/util');
const selector = require ('../main/pageObjects/selector');
const timeout = 60000;

beforeAll(async () => {
    await page.goto(URL,{waitUntil: 'domcontentloaded',timeout: 0});
},timeout);

describe('Test Ad Invoke using interceptor',() => {
    test('ad invoke using interceptor',async () => {
        //Switching on interceptor and capturing google ads
        await page.setRequestInterception(true);
        const hold1 = resolveCall('/gampad/ads');
        await page.reload({waitUntil: 'networkidle2'});
        let bool1 = await hold1;
        console.log(bool1);
        assert.isTrue(bool1);
        

         //obtain the page url
        const url1 = await getPageUrl(selector.selector.next);
        console.log('URL 1: ',url1);

        //click the next page
        await elementClick(selector.selector.next);
        await page.waitFor(2000);
        //check if page has navigated
        const url2 = await getPageUrl(selector.selector.next);
        console.log('URL 2: ',url2);
        assert.notEqual(url1,url2);

        page.removeAllListeners('request');
        //Capturing google ads here again
        const hold2 = resolveCall('/gampad/ads');
        await page.reload({waitUntil: 'networkidle2'});
        let bool2 = await hold2;
        console.log(bool2);
        assert.isTrue(bool2);
        await page.setRequestInterception(false);
        
    },timeout);
});