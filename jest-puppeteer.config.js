module.exports = {
    launch: {
        headless: false,
        slowMo: false,
        devtools: true,
        ignoreHTTPSErrors: true,
        args: [
            '--start-fullscreen',
            '--window-size=1920,1040',
            '--no-sandbox'
        ]
    }
}